
/*
 *
 * Usage: java Receiver <receiver_port> <filename>
 * 
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Receiver {
      public enum ReceiverState {
            NIL, LISTEN, SYN_RECEIVED, ESTABLISHED, FIN_WAIT_1, FIN_WAIT_2, CLOSE_WAIT, CLOSING, LAST_ACK, TIME_WAIT,
            CLOSED;
      };

      public static void main(String[] args) throws Exception {
            int receiver_port = 0;
            String filename;
            if (args.length == 2) {
                  try {
                        receiver_port = Integer.parseInt(args[0]);
                        filename = args[1];
                  } catch (Exception e) {
                        System.out.println(e);
                        return;
                  }
            } else {
                  System.out.println("Usage: java Receiver <receiver_port> <filename>");
                  return;
            }
            STPPacketController.log_output_stream = new FileOutputStream("Receiver_log.txt");
            DatagramSocket socket = new DatagramSocket(receiver_port);
            STPPacketController.setSocket(socket);
            FileOutputStream file_output_stream = new FileOutputStream(filename);
            ReceiverState state = ReceiverState.NIL;
            ReceiverState previous_state = ReceiverState.CLOSED;
            int sequence_number = 0;
            int acknowledgement_number = 0;
            Queue<STPPacketController> receiving_queue = new LinkedList<STPPacketController>();

            int total_file_size = 0;

            while (state != ReceiverState.CLOSED) {
                  switch (state) {
                  case NIL: {
                        STPPacketController.setStart(System.currentTimeMillis());
                        state = ReceiverState.LISTEN;
                        break;
                  }
                  case LISTEN: {
                        STPPacketController received_packet = STPPacketController.receivePacketAndLog(0);
                        if (received_packet.getHeaderController().getSYN()
                                    && !received_packet.getHeaderController().getACK()) {
                              acknowledgement_number = received_packet.getHeaderController().getSequenceNumber() + 1;
                              state = ReceiverState.SYN_RECEIVED;
                        }
                        break;
                  }
                  case SYN_RECEIVED: {
                        boolean SYN = true;
                        boolean RST = false;
                        boolean ACK = true;
                        boolean FIN = false;
                        byte[] data = null;
                        STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number,
                                    SYN, RST, ACK, FIN, data);
                        packet.sendPacket();

                        STPPacketController received_packet = STPPacketController.receivePacketAndLog(3000);
                        if (received_packet == null) {
                              break;
                        }
                        if (!received_packet.getHeaderController().getSYN()
                                    && received_packet.getHeaderController().getACK()) {
                              acknowledgement_number = received_packet.getHeaderController().getSequenceNumber();
                              sequence_number = received_packet.getHeaderController().getAcknowledgeNumber();
                              state = ReceiverState.ESTABLISHED;
                        }
                        break;
                  }
                  case ESTABLISHED: {
                        STPPacketController received_packet = STPPacketController.receivePacketAndLog(0);
                        if (received_packet == null) {
                              break;
                        }
                        if (received_packet.getHeaderController().getFIN()) {
                              acknowledgement_number = received_packet.getHeaderController().getSequenceNumber() + 1;
                              state = ReceiverState.CLOSE_WAIT;
                              break;
                        }

                        receiving_queue.add(received_packet);
                        Collections.sort((LinkedList<STPPacketController>) receiving_queue, new SortPacketBySequence());

                        int pre_ack = acknowledgement_number;

                        while (receiving_queue.peek() != null && receiving_queue.peek().getHeaderController()
                                    .getSequenceNumber() <= acknowledgement_number) {
                              if (receiving_queue.peek().getHeaderController()
                                          .getSequenceNumber() < acknowledgement_number) {
                                    STPPacketController.duplicated_data_received += 1;
                                    receiving_queue.poll();
                                    continue;
                              }

                              sequence_number = receiving_queue.peek().getHeaderController().getAcknowledgeNumber();
                              acknowledgement_number = receiving_queue.peek().getHeaderController().getSequenceNumber()
                                          + receiving_queue.peek().getDataLength();
                              total_file_size += receiving_queue.peek().getDataLength();
                              file_output_stream.write(receiving_queue.poll().getRawData());
                        }

                        boolean SYN = false;
                        boolean RST = false;
                        boolean ACK = true;
                        boolean FIN = false;
                        byte[] data = null;
                        STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number,
                                    SYN, RST, ACK, FIN, data);
                        if (pre_ack == acknowledgement_number) {
                              STPPacketController.duplicated_ACKing += 1;
                              packet.re_ACKing();
                        }
                        packet.sendPacket();

                        break;
                  }
                  case CLOSE_WAIT: {
                        boolean SYN = false;
                        boolean RST = false;
                        boolean ACK = true;
                        boolean FIN = false;
                        byte[] data = null;
                        STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number,
                                    SYN, RST, ACK, FIN, data);
                        packet.sendPacket();
                        state = ReceiverState.LAST_ACK;
                        break;
                  }
                  case LAST_ACK: {
                        boolean SYN = false;
                        boolean RST = false;
                        boolean ACK = false;
                        boolean FIN = true;
                        byte[] data = null;
                        STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number,
                                    SYN, RST, ACK, FIN, data);
                        packet.sendPacket();

                        STPPacketController received_packet = STPPacketController.receivePacketAndLog(3000);
                        if (received_packet == null) {
                              state = ReceiverState.CLOSED;
                              break;
                        }
                        if (received_packet.getHeaderController().getACK() && received_packet.getHeaderController()
                                    .getAcknowledgeNumber() == sequence_number + 1) {
                              state = ReceiverState.CLOSED;
                        }
                        break;
                  }
                  default:
                        break;
                  }
                  if (previous_state != state) {
                        System.out.println(state);
                        previous_state = state;
                  }
            }

            try {
                  STPPacketController.log_output_stream
                              .write("=============================================================\n".getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream.write(String
                              .format("%-50s %8d\n", "Amount of Data Received (bytes)", total_file_size).getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream.write(
                              String.format("%-50s %8d\n", "Total segments received", STPPacketController.received)
                                          .getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream.write(
                              String.format("%-50s %8d\n", "Data segments received", STPPacketController.data_received)
                                          .getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream.write(String.format("%-50s %8d\n",
                              "Data Segments with bit errors", STPPacketController.bit_errors_received).getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream
                              .write(String.format("%-50s %8d\n", "Duplicate data segments received",
                                          STPPacketController.duplicated_data_received).getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream.write(
                              String.format("%-50s %8d\n", "Duplicate Acks sent", STPPacketController.duplicated_ACKing)
                                          .getBytes());
            } catch (IOException e) {

            }

            try {
                  STPPacketController.log_output_stream
                              .write("=============================================================\n".getBytes());
            } catch (IOException e) {

            }
      }
}

class SortPacketBySequence implements Comparator<STPPacketController> {
      public int compare(STPPacketController a, STPPacketController b) {
            return a.getHeaderController().getSequenceNumber() - b.getHeaderController().getSequenceNumber();
      }
}