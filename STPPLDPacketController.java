import java.io.*;
import java.net.*;
import java.util.*;

public class STPPLDPacketController extends STPPacketController {
    private static float pDrop = 0.0f;

    public static void setDrop(float pDrop) {
        STPPLDPacketController.pDrop = pDrop;
    }

    private static float pDuplicate = 0.0f;

    public static void setDuplicate(float pDuplicate) {
        STPPLDPacketController.pDuplicate = pDuplicate;
    }

    private static float pCorrupt = 0.0f;

    public static void setCorrupt(float pCorrupt) {
        STPPLDPacketController.pCorrupt = pCorrupt;
    }

    private static float pOrder = 0.0f;
    private static int maxOrder = 0;

    public static void setOrder(float pOrder, int maxOrder) {
        STPPLDPacketController.pOrder = pOrder;
        STPPLDPacketController.maxOrder = maxOrder;
    }

    private static float pDelay = 0.0f;
    private static int maxDelay = 0;

    public static void setDelay(float pDelay, int maxDelay) {
        STPPLDPacketController.pDelay = pDelay;
        STPPLDPacketController.maxDelay = maxDelay;
    }

    private static long seed = 0;

    public static void setSeed(long seed) {
        STPPLDPacketController.seed = seed;
    }

    private boolean bypass = false;
    private static boolean reordering = false;
    private static STPPLDPacketController reorder_packet = null;
    private static int reorder_count = 0;
    protected static Random random = null;

    public static void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
            }
        }).start();
    }

    public STPPLDPacketController(byte[] packet, boolean bypass) {
        super(packet);
        this.bypass = bypass;
    }

    public STPPLDPacketController(int sequence_number, int acknowledgement_number, boolean SYN, boolean RST,
            boolean ACK, boolean FIN, byte[] data, boolean bypass) {
        super(sequence_number, acknowledgement_number, SYN, RST, ACK, FIN, data);
        this.bypass = bypass;
    }

    protected boolean sendDroppingPacket() {
        if (STPPLDPacketController.random.nextFloat() < STPPLDPacketController.pDrop) {
            STPPacketController.dropped += 1;
            STPPacketController.pld_handled += 1;
            if (STPPacketController.printing) {
                try {
                    STPPacketController.log_output_stream.write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                            "drop" + (this.getRetransmitting() ? "/RXT" : ""),
                            (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                            (this.getDataLength() > 0 ? "D"
                                    : ((this.getHeaderController().getSYN() ? "S" : "")
                                            + (this.getHeaderController().getACK() ? "A" : "")
                                            + (this.getHeaderController().getFIN() ? "F" : ""))),
                            this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                            this.getHeaderController().getAcknowledgeNumber()).getBytes());
                } catch (IOException e) {

                }
            }
            return true;
        }
        return false;
    }

    protected boolean sendDuplicatePacket() {
        if (STPPLDPacketController.random.nextFloat() < STPPLDPacketController.pDuplicate) {
            if (super.sendPacket() && super.sendSuperPacket()) {
                STPPacketController.pld_handled += 1;
                STPPacketController.pld_handled += 1;
                STPPacketController.duplicated += 1;
                if (STPPacketController.printing) {
                    try {
                        STPPacketController.log_output_stream
                                .write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                                        "snd" + (this.getRetransmitting() ? "/RXT" : "") + "/dup",
                                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                        (this.getDataLength() > 0 ? "D"
                                                : ((this.getHeaderController().getSYN() ? "S" : "")
                                                        + (this.getHeaderController().getACK() ? "A" : "")
                                                        + (this.getHeaderController().getFIN() ? "F" : ""))),
                                        this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                                        this.getHeaderController().getAcknowledgeNumber()).getBytes());
                    } catch (IOException e) {

                    }
                }
                return true;
            }
        }
        return false;
    }

    protected boolean sendCorruptPacket() {
        if (STPPLDPacketController.random.nextFloat() < STPPLDPacketController.pCorrupt) {
            try {
                ByteArrayOutputStream byte_array_output_stream = new ByteArrayOutputStream();
                byte_array_output_stream.write(this.header_controller.getHeader());
                byte[] data_flipped = Arrays.copyOf(this.data, this.data.length);
                if (data_flipped != null && data_flipped.length != 0) {
                    int flipping_byte = (int) (STPPLDPacketController.random.nextFloat() * data_flipped.length);
                    int flipping_bit = (int) (STPPLDPacketController.random.nextFloat() * 8);
                    data_flipped[flipping_byte] = (byte) (data_flipped[flipping_byte] ^ (1 << flipping_bit));
                    byte_array_output_stream.write(data_flipped);
                }
                byte[] flipped_packet = byte_array_output_stream.toByteArray();
                byte_array_output_stream.close();
                STPPacketController.socket.send(new DatagramPacket(flipped_packet, flipped_packet.length,
                        STPPacketController.receiver_host, STPPacketController.receiver_port));
                STPPacketController.pld_handled += 1;
                STPPacketController.corrupted += 1;
                if (STPPacketController.printing) {
                    try {
                        STPPacketController.log_output_stream
                                .write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                                        "snd" + (this.getRetransmitting() ? "/RXT" : "") + "/corr",
                                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                        (this.getDataLength() > 0 ? "D"
                                                : ((this.getHeaderController().getSYN() ? "S" : "")
                                                        + (this.getHeaderController().getACK() ? "A" : "")
                                                        + (this.getHeaderController().getFIN() ? "F" : ""))),
                                        this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                                        this.getHeaderController().getAcknowledgeNumber()).getBytes());
                    } catch (IOException e) {

                    }
                }
                return true;
            } catch (IOException e) {

            }
        }
        return false;
    }

    protected boolean sendReorderPacket() {
        if (STPPLDPacketController.random.nextFloat() < STPPLDPacketController.pOrder) {
            if (STPPLDPacketController.reordering) {
                return super.sendSuperPacket();
            }
            STPPLDPacketController.reordering = true;
            STPPLDPacketController.reorder_count = 0;
            STPPLDPacketController.reorder_packet = this;
            STPPacketController.pld_handled += 1;
            STPPacketController.reordered += 1;
            return true;
        }
        return false;
    }

    protected boolean sendDelayPacket() {
        if (STPPLDPacketController.random.nextFloat() < STPPLDPacketController.pDelay) {
            int delay = (int) (STPPLDPacketController.random.nextFloat() * STPPLDPacketController.maxDelay);
            STPPLDPacketController.setTimeout(() -> {
                this.sendSuperPacket();
                if (STPPacketController.printing) {
                    try {
                        STPPacketController.log_output_stream
                                .write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                                        "snd" + (this.getRetransmitting() ? "/RXT" : "") + "/dely",
                                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                        (this.getDataLength() > 0 ? "D"
                                                : ((this.getHeaderController().getSYN() ? "S" : "")
                                                        + (this.getHeaderController().getACK() ? "A" : "")
                                                        + (this.getHeaderController().getFIN() ? "F" : ""))),
                                        this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                                        this.getHeaderController().getAcknowledgeNumber()).getBytes());
                    } catch (IOException e) {

                    }
                }
            }, delay);
            STPPacketController.pld_handled += 1;
            STPPacketController.delayed += 1;
            return true;
        }
        return false;
    }

    protected boolean sendSuperPacket() {
        return super.sendSuperPacket();
    }

    protected boolean sendNormalPacket() {
        STPPacketController.pld_handled += 1;
        if (STPPacketController.printing) {
            try {
                STPPacketController.log_output_stream.write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                        "snd" + (this.getRetransmitting() ? "/RXT" : ""),
                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                        (this.getDataLength() > 0 ? "D"
                                : ((this.getHeaderController().getSYN() ? "S" : "")
                                        + (this.getHeaderController().getACK() ? "A" : "")
                                        + (this.getHeaderController().getFIN() ? "F" : ""))),
                        this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                        this.getHeaderController().getAcknowledgeNumber()).getBytes());
            } catch (IOException e) {

            }
        }
        return this.sendSuperPacket();
    }

    public boolean sendPacket() {
        if (STPPLDPacketController.random == null) {
            STPPLDPacketController.random = new Random(STPPLDPacketController.seed);
        }
        if (this.bypass && !this.sendNormalPacket()) {
            return false;
        } else if (!this.bypass && !this.sendDroppingPacket() && !this.sendDuplicatePacket()
                && !this.sendCorruptPacket() && !this.sendReorderPacket() && !this.sendDelayPacket()
                && !this.sendNormalPacket()) {
            return false;
        } else {
            if (STPPLDPacketController.reordering
                    && ++STPPLDPacketController.reorder_count > STPPLDPacketController.maxOrder) {
                STPPLDPacketController.reorder_packet.sendSuperPacket();
                STPPLDPacketController.reordering = false;
                STPPLDPacketController.reorder_count = 0;
                STPPLDPacketController.reorder_packet = null;
                if (STPPacketController.printing) {
                    try {
                        STPPacketController.log_output_stream
                                .write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n",
                                        "snd" + (this.getRetransmitting() ? "/RXT" : "") + "/rord",
                                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                        (this.getDataLength() > 0 ? "D"
                                                : ((this.getHeaderController().getSYN() ? "S" : "")
                                                        + (this.getHeaderController().getACK() ? "A" : "")
                                                        + (this.getHeaderController().getFIN() ? "F" : ""))),
                                        this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                                        this.getHeaderController().getAcknowledgeNumber()).getBytes());
                    } catch (IOException e) {

                    }
                }
            }
            this.timing = System.currentTimeMillis();
            return true;
        }
    }

    public void printPacket() {
        super.printPacket();
        System.out.println("bypass: " + this.bypass);
    }
}