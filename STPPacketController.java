
/*
 * Design of Simple Transport Protocol:
 * Packet Model:
 * {
 *     byte[16] header;
 *     byte[] data;
 * }
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

public class STPPacketController {

    public static FileOutputStream log_output_stream = null;

    protected static boolean printing = true;

    protected static long start_time = 0;

    public static void setStart(long start_time) {
        STPPacketController.start_time = start_time;
    }

    public static long getStart() {
        return STPPacketController.start_time;
    }

    protected static DatagramSocket socket = null;

    public static void setSocket(DatagramSocket socket) {
        STPPacketController.socket = socket;
    }

    protected static InetAddress receiver_host = null;

    public static void setReceiverHost(InetAddress receiver_host) {
        STPPacketController.receiver_host = receiver_host;
    }

    protected static int receiver_port = 0;

    public static void setReceiverPort(int receiver_port) {
        STPPacketController.receiver_port = receiver_port;
    }

    protected STPHeaderController header_controller = null;

    public STPHeaderController getHeaderController() {
        return this.header_controller;
    }

    protected byte[] packet = null;

    public int getPacketLength() {
        if (this.packet == null) {
            return 0;
        }
        return this.packet.length;
    }

    protected byte[] data = null;

    public int getDataLength() {
        if (this.data == null) {
            return 0;
        }
        return this.data.length;
    }

    public byte[] getRawData() {
        return this.data;
    }

    protected long timing = 0;

    public void setTiming(long timing) {
        this.timing = timing;
    }

    public long getTiming() {
        return this.timing;
    }

    protected boolean retransmitting = false;

    public void retransmitting() {
        this.retransmitting = true;
    }

    public boolean getRetransmitting() {
        return this.retransmitting;
    }

    protected boolean re_ack = false;

    public void re_ACKing() {
        this.re_ack = true;
    }

    public boolean getReACKing() {
        return this.re_ack;
    }

    public static int transmitted = 0;
    public static int pld_handled = 0;
    public static int dropped = 0;
    public static int corrupted = 0;
    public static int reordered = 0;
    public static int duplicated = 0;
    public static int delayed = 0;
    public static int timeout_retransmitted = 0;
    public static int fast_retransmitted = 0;
    public static int rcv_da = 0;

    public static int received = 0;
    public static int data_received = 0;
    public static int bit_errors_received = 0;
    public static int duplicated_data_received = 0;
    public static int duplicated_ACKing = 0;

    public STPPacketController(byte[] packet) {
        this.packet = packet;
        this.header_controller = new STPHeaderController(
                Arrays.copyOfRange(this.packet, 0, STPHeaderController.headerLength()));
        this.data = Arrays.copyOfRange(this.packet, STPHeaderController.headerLength(), packet.length);
    }

    public STPPacketController(int sequence_number, int acknowledgement_number, boolean SYN, boolean RST, boolean ACK,
            boolean FIN, byte[] data) {
        byte[] adata = data;
        if (adata == null || adata.length <= 0) {
            adata = new byte[] { 00 };
        }
        Checksum checksum = new CRC32();
        checksum.reset();
        checksum.update(adata, 0, adata.length);
        long checksum_value = checksum.getValue();
        this.header_controller = new STPHeaderController(sequence_number, acknowledgement_number, SYN, RST, ACK, FIN,
                checksum_value);
        this.data = data;
        try {
            ByteArrayOutputStream byte_array_output_stream = new ByteArrayOutputStream();
            byte_array_output_stream.write(this.header_controller.getHeader());
            if (this.data != null && this.data.length != 0) {
                byte_array_output_stream.write(this.data);
            }
            this.packet = byte_array_output_stream.toByteArray();
            byte_array_output_stream.close();
        } catch (IOException e) {

        }
    }

    protected boolean sendSuperPacket() {
        try {
            STPPacketController.socket.send(new DatagramPacket(this.packet, this.packet.length,
                    STPPacketController.receiver_host, STPPacketController.receiver_port));
        } catch (IOException e) {
            return false;
        }
        STPPacketController.transmitted += 1;
        this.timing = System.currentTimeMillis();
        return true;
    }

    public boolean sendPacket() {
        if (STPPacketController.printing) {
            try {
                STPPacketController.log_output_stream.write(
                        String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n", "snd" + (this.getReACKing() ? "/DA" : ""),
                                (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                (this.getDataLength() > 0 ? "D"
                                        : ((this.getHeaderController().getSYN() ? "S" : "")
                                                + (this.getHeaderController().getACK() ? "A" : "")
                                                + (this.getHeaderController().getFIN() ? "F" : ""))),
                                this.getHeaderController().getSequenceNumber(), this.getDataLength(),
                                this.getHeaderController().getAcknowledgeNumber()).getBytes());
            } catch (IOException e) {

            }
        }
        return this.sendSuperPacket();
    }

    public static STPPacketController receivePacketWithoutLog(int timeout) {
        DatagramPacket data_packet_receiving = new DatagramPacket(new byte[4096], 4096);
        try {
            STPPacketController.socket.setSoTimeout(timeout);
            STPPacketController.socket.receive(data_packet_receiving);
            if (STPPacketController.receiver_host == null) {
                STPPacketController.receiver_host = data_packet_receiving.getAddress();
            }
            STPPacketController.received += 1;
            if (STPPacketController.receiver_port == 0) {
                STPPacketController.receiver_port = data_packet_receiving.getPort();
            }
            STPPacketController pack = new STPPacketController(
                    Arrays.copyOfRange(data_packet_receiving.getData(), 0, data_packet_receiving.getLength()));
            byte[] adata = pack.getRawData();
            if (adata == null || adata.length <= 0) {
                adata = new byte[] { 00 };
            } else {
                STPPacketController.data_received += 1;
            }
            Checksum checksum = new CRC32();
            checksum.reset();
            checksum.update(adata, 0, adata.length);
            long checksum_value = checksum.getValue();
            if (pack.header_controller.getChecksum() != checksum_value) {
                STPPacketController.bit_errors_received += 1;
                if (STPPacketController.printing) {
                    try {
                        STPPacketController.log_output_stream
                                .write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n", "rcv/corr",
                                        (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                                        (pack.getDataLength() > 0 ? "D"
                                                : ((pack.getHeaderController().getSYN() ? "S" : "")
                                                        + (pack.getHeaderController().getACK() ? "A" : "")
                                                        + (pack.getHeaderController().getFIN() ? "F" : ""))),
                                        pack.getHeaderController().getSequenceNumber(), pack.getDataLength(),
                                        pack.getHeaderController().getAcknowledgeNumber()).getBytes());
                    } catch (IOException e) {

                    }
                }
                return null;
            }
            return pack;
        } catch (IOException e) {
            return null;
        }
    }

    public static STPPacketController receivePacketAndLog(int timeout) {
        STPPacketController received_packet = STPPacketController.receivePacketWithoutLog(timeout);
        if (received_packet == null) {
            return null;
        }
        try {
            STPPacketController.log_output_stream.write(String.format("%-16s %-14.2f %-16s %-16s %-16s %s\n", "rcv",
                    (System.currentTimeMillis() - STPPacketController.start_time) / 1000f,
                    (received_packet.getDataLength() > 0 ? "D"
                            : ((received_packet.getHeaderController().getSYN() ? "S" : "")
                                    + (received_packet.getHeaderController().getACK() ? "A" : "")
                                    + (received_packet.getHeaderController().getFIN() ? "F" : ""))),
                    received_packet.getHeaderController().getSequenceNumber(), received_packet.getDataLength(),
                    received_packet.getHeaderController().getAcknowledgeNumber()).getBytes());
        } catch (IOException e) {

        }
        return received_packet;
    }

    public void printPacket() {
        this.header_controller.print_header();
        System.out.println("data length: " + (this.data == null ? 0 : this.data.length));
        System.out.println("packet length: " + this.packet.length);
    }
}