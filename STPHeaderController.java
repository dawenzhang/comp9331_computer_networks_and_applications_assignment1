
/*
 * Design of Simple Transport Protocol:
 * Header Model:
 * {
 *     int sequence_number;
 *     int acknowledgement_number;
 *     byte SYN;
 *     byte RST;
 *     byte ACK;
 *     byte FIN;
 *     long checksum;
 * }
 * of size 20 bytes;
 */

import java.io.*;
import java.util.*;
import java.nio.ByteBuffer;

public class STPHeaderController {

    private static int header_length = 20;

    public static int headerLength() {
        return STPHeaderController.header_length;
    }

    private int sequence_number;

    public int getSequenceNumber() {
        return this.sequence_number;
    }

    private int acknowledgement_number;

    public int getAcknowledgeNumber() {
        return this.acknowledgement_number;
    }

    private boolean SYN = false;

    public boolean getSYN() {
        return this.SYN;
    }

    private boolean RST = false;

    public boolean getRST() {
        return this.RST;
    }

    private boolean ACK = false;

    public boolean getACK() {
        return this.ACK;
    }

    private boolean FIN = false;

    public boolean getFIN() {
        return this.FIN;
    }

    private long checksum = 0;

    public long getChecksum() {
        return this.checksum;
    }

    private byte[] header = null;

    public STPHeaderController(byte[] header) {
        this.header = header;
        int size_acc = 0;
        this.sequence_number = ByteBuffer.wrap(Arrays.copyOfRange(this.header, size_acc, size_acc + 4)).getInt();
        size_acc += 4;
        this.acknowledgement_number = ByteBuffer.wrap(Arrays.copyOfRange(this.header, size_acc, size_acc + 4)).getInt();
        size_acc += 4;
        this.SYN = Arrays.copyOfRange(this.header, size_acc, size_acc + 1)[0] == 1;
        size_acc += 1;
        this.RST = Arrays.copyOfRange(this.header, size_acc, size_acc + 1)[0] == 1;
        size_acc += 1;
        this.ACK = Arrays.copyOfRange(this.header, size_acc, size_acc + 1)[0] == 1;
        size_acc += 1;
        this.FIN = Arrays.copyOfRange(this.header, size_acc, size_acc + 1)[0] == 1;
        size_acc += 1;
        this.checksum = ByteBuffer.wrap(Arrays.copyOfRange(this.header, size_acc, size_acc + 8)).getLong();
        size_acc += 8;
    }

    public STPHeaderController(int sequence_number, int acknowledgement_number, boolean SYN, boolean RST, boolean ACK,
            boolean FIN, long checksum) {
        this.sequence_number = sequence_number;
        this.acknowledgement_number = acknowledgement_number;
        this.SYN = SYN;
        this.RST = RST;
        this.ACK = ACK;
        this.FIN = FIN;
        this.checksum = checksum;
        try {
            ByteArrayOutputStream byte_array_output_stream = new ByteArrayOutputStream();
            byte_array_output_stream.write(ByteBuffer.allocate(4).putInt(this.sequence_number).array());
            byte_array_output_stream.write(ByteBuffer.allocate(4).putInt(this.acknowledgement_number).array());
            byte_array_output_stream.write(this.SYN ? new byte[] { 01 } : new byte[] { 00 });
            byte_array_output_stream.write(this.RST ? new byte[] { 01 } : new byte[] { 00 });
            byte_array_output_stream.write(this.ACK ? new byte[] { 01 } : new byte[] { 00 });
            byte_array_output_stream.write(this.FIN ? new byte[] { 01 } : new byte[] { 00 });
            byte_array_output_stream.write(ByteBuffer.allocate(8).putLong(this.checksum).array());
            this.header = byte_array_output_stream.toByteArray();
            byte_array_output_stream.close();
        } catch (IOException e) {

        }
    }

    public byte[] getHeader() {
        if (this.header == null) {
            try {
                ByteArrayOutputStream byte_array_output_stream = new ByteArrayOutputStream();
                byte_array_output_stream.write(ByteBuffer.allocate(4).putInt(this.sequence_number).array());
                byte_array_output_stream.write(ByteBuffer.allocate(4).putInt(this.acknowledgement_number).array());
                byte_array_output_stream.write(this.SYN ? new byte[] { 01 } : new byte[] { 00 });
                byte_array_output_stream.write(this.RST ? new byte[] { 01 } : new byte[] { 00 });
                byte_array_output_stream.write(this.ACK ? new byte[] { 01 } : new byte[] { 00 });
                byte_array_output_stream.write(this.FIN ? new byte[] { 01 } : new byte[] { 00 });
                byte_array_output_stream.write(ByteBuffer.allocate(8).putLong(this.checksum).array());
                this.header = byte_array_output_stream.toByteArray();
                byte_array_output_stream.close();
            } catch (IOException e) {

            }
        }
        return this.header;
    }

    public void print_header() {
        System.out.println("SYN: " + this.SYN);
        System.out.println("RST: " + this.RST);
        System.out.println("ACK: " + this.ACK);
        System.out.println("FIN: " + this.FIN);
        System.out.println("sequence number: " + this.sequence_number);
        System.out.println("acknowledgement number: " + this.acknowledgement_number);
        System.out.println("checksum: " + this.checksum);
    }
}