
/*
 *
 * Usage: java Sender <receiver_host_ip> <receiver_port> <filename> <MWS> <MSS> <gamma>
 * 
 * 
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.time.*;

public class Sender {
    public enum SenderState {
        NIL, SYN_SENT, ESTABLISHED, FIN_WAIT_1, FIN_WAIT_2, CLOSE_WAIT, CLOSING, LAST_ACK, TIME_WAIT, CLOSED;
    };

    static int service_port = 8090;

    public static void main(String[] args) throws Exception {
        InetAddress receiver_host;
        int receiver_port;
        String filename;
        int MWS;
        int MSS;
        int gamma;
        float pDrop;
        float pDuplicate;
        float pCorrupt;
        float pOrder;
        int maxOrder;
        float pDelay;
        int maxDelay;
        long seed;
        if (args.length == 14) {
            try {
                receiver_host = InetAddress.getByName(args[0]);
                STPPLDPacketController.setReceiverHost(receiver_host);
                receiver_port = Integer.parseInt(args[1]);
                STPPLDPacketController.setReceiverPort(receiver_port);
                filename = args[2];
                MWS = Integer.parseInt(args[3]);
                MSS = Integer.parseInt(args[4]);
                gamma = Integer.parseInt(args[5]);
                pDrop = Float.parseFloat(args[6]);
                STPPLDPacketController.setDrop(pDrop);
                pDuplicate = Float.parseFloat(args[7]);
                STPPLDPacketController.setDuplicate(pDuplicate);
                pCorrupt = Float.parseFloat(args[8]);
                STPPLDPacketController.setCorrupt(pCorrupt);
                pOrder = Float.parseFloat(args[9]);
                maxOrder = Integer.parseInt(args[10]);
                STPPLDPacketController.setOrder(pOrder, maxOrder);
                pDelay = Float.parseFloat(args[11]);
                maxDelay = Integer.parseInt(args[12]);
                STPPLDPacketController.setDelay(pDelay, maxDelay);
                seed = Long.parseLong(args[13]);
                STPPLDPacketController.setSeed(seed);
            } catch (Exception e) {
                System.out.println(e);
                return;
            }
        } else {
            System.out.println(
                    "Usage: java Sender <receiver_host_ip> <receiver_port> <filename> <MWS> <MSS> <gamma> <pDrop> <pDuplicate> <pCorrupt> <pOrder> <maxOrder> <pDelay> <maxDelay> <seed>");
            return;
        }

        STPPacketController.log_output_stream = new FileOutputStream("Sender_log.txt");
        DatagramSocket socket = new DatagramSocket(service_port);
        STPPacketController.socket = socket;
        InputStream file_input_stream = new FileInputStream(filename);
        byte[] file_piece = new byte[MSS];
        int sequence_number = 0;
        int acknowledgement_number = 0;
        Queue<STPPacketController> sent_to_ack = new LinkedList<STPPacketController>();
        Queue<STPPacketController> sending = new LinkedList<STPPacketController>();
        int sent_length = 0;
        int sending_length = 0;
        int retrans_count = 0;
        int retrans_seq = 0;

        SenderState state = SenderState.NIL;
        SenderState previous_state = SenderState.CLOSED;

        int total_file_size = 0;

        long estimated_RTT = 500;
        long dev_RTT = 250;
        STPPacketController.setStart(System.currentTimeMillis());

        while (state != SenderState.CLOSED) {
            switch (state) {
            case NIL: {
                boolean SYN = true;
                boolean RST = false;
                boolean ACK = false;
                boolean FIN = false;
                byte[] data = null;
                boolean bypass = true;
                STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number, SYN, RST,
                        ACK, FIN, data);
                packet.sendPacket();
                STPPacketController received_packet = STPPacketController.receivePacketAndLog(
                        getTimeout(getTimeInterval(estimated_RTT, dev_RTT, gamma), packet.getTiming()));
                if (received_packet != null && received_packet.getHeaderController().getSYN()
                        && received_packet.getHeaderController().getACK()) {
                    if (!packet.getRetransmitting()) {
                        dev_RTT = getDevRTT(estimated_RTT, dev_RTT,
                                (long) (System.currentTimeMillis() - packet.getTiming()));
                        estimated_RTT = getEstimatedRTT(estimated_RTT,
                                (long) (System.currentTimeMillis() - packet.getTiming()));
                    }
                    acknowledgement_number = received_packet.getHeaderController().getSequenceNumber() + 1;
                    sequence_number = received_packet.getHeaderController().getAcknowledgeNumber();
                    state = SenderState.SYN_SENT;
                }
                break;
            }
            case SYN_SENT: {
                boolean SYN = false;
                boolean RST = false;
                boolean ACK = true;
                boolean FIN = false;
                byte[] data = null;
                boolean bypass = true;
                STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number, SYN, RST,
                        ACK, FIN, data);
                packet.sendPacket();
                state = SenderState.ESTABLISHED;
                break;
            }
            case ESTABLISHED: {
                while (sending_length < MWS) {
                    int file_piece_length = file_input_stream.read(file_piece);
                    if (file_piece_length <= 0) {
                        break;
                    }
                    total_file_size += file_piece_length;
                    boolean SYN = false;
                    boolean RST = false;
                    boolean ACK = true;
                    boolean FIN = false;
                    byte[] data = Arrays.copyOfRange(file_piece, 0, file_piece_length);
                    boolean bypass = false;
                    STPPLDPacketController packet = new STPPLDPacketController(sequence_number, acknowledgement_number,
                            SYN, RST, ACK, FIN, data, bypass);
                    sending.add(packet);
                    sending_length += packet.getDataLength();
                    sequence_number += packet.getDataLength();
                }
                if (sent_length <= 0 && sending_length <= 0) {
                    state = SenderState.FIN_WAIT_1;
                    break;
                }
                if (sent_length <= 0) {
                    while (sending_length > 0 && sending.peek().getDataLength() + sent_length <= MWS) {
                        STPPacketController packet = sending.poll();
                        sending_length -= packet.getDataLength();
                        packet.sendPacket();
                        sent_to_ack.add(packet);
                        sent_length += packet.getDataLength();
                    }
                }
                STPPacketController received_packet = STPPacketController.receivePacketWithoutLog(
                        getTimeout(getTimeInterval(estimated_RTT, dev_RTT, gamma), sent_to_ack.peek().getTiming()));
                if (received_packet != null) {
                    boolean logged = false;
                    if (!received_packet.getHeaderController().getSYN()
                            && received_packet.getHeaderController().getACK()) {
                        if (sent_to_ack.peek() != null && !sent_to_ack.peek().getRetransmitting()
                                && received_packet.getHeaderController().getAcknowledgeNumber() == sent_to_ack.peek()
                                        .getHeaderController().getSequenceNumber()
                                        + sent_to_ack.peek().getDataLength()) {
                            dev_RTT = getDevRTT(estimated_RTT, dev_RTT,
                                    (long) (System.currentTimeMillis() - sent_to_ack.peek().getTiming()));
                            estimated_RTT = getEstimatedRTT(estimated_RTT,
                                    (long) (System.currentTimeMillis() - sent_to_ack.peek().getTiming()));
                        }
                        while (sent_to_ack.peek() != null && received_packet.getHeaderController()
                                .getAcknowledgeNumber() >= sent_to_ack.peek().getHeaderController().getSequenceNumber()
                                        + sent_to_ack.peek().getDataLength()) {
                            acknowledgement_number = received_packet.getHeaderController().getSequenceNumber()
                                    + received_packet.getDataLength();
                            sent_length -= sent_to_ack.poll().getDataLength();
                        }
                        if (sent_to_ack.peek() != null
                                && received_packet.getHeaderController().getAcknowledgeNumber() == sent_to_ack.peek()
                                        .getHeaderController().getSequenceNumber()) {
                            if (retrans_seq == sent_to_ack.peek().getHeaderController().getSequenceNumber()) {
                                logged = true;
                                STPPacketController.rcv_da += 1;
                                try {
                                    STPPacketController.log_output_stream.write(String.format(
                                            "%-16s %-14.2f %-16s %-16s %-16s %s\n", "rcv/DA",
                                            (System.currentTimeMillis() - STPPacketController.getStart()) / 1000f,
                                            (received_packet.getDataLength() > 0 ? "D"
                                                    : ((received_packet.getHeaderController().getSYN() ? "S" : "")
                                                            + (received_packet.getHeaderController().getACK() ? "A"
                                                                    : "")
                                                            + (received_packet.getHeaderController().getFIN() ? "F"
                                                                    : ""))),
                                            received_packet.getHeaderController().getSequenceNumber(),
                                            received_packet.getDataLength(),
                                            received_packet.getHeaderController().getAcknowledgeNumber()).getBytes());
                                } catch (IOException e) {

                                }
                                if (++retrans_count >= 3) {
                                    STPPacketController.fast_retransmitted += 1;
                                    sent_to_ack.peek().retransmitting();
                                    sent_to_ack.peek().sendPacket();
                                    retrans_seq = 0;
                                    retrans_count = 0;
                                }
                            } else {
                                retrans_seq = sent_to_ack.peek().getHeaderController().getSequenceNumber();
                                retrans_count = 0;
                            }
                        }
                    } else if (received_packet.getHeaderController().getSYN()
                            && received_packet.getHeaderController().getACK()) {
                        state = SenderState.SYN_SENT;
                    }
                    if (logged == false) {
                        try {
                            STPPacketController.log_output_stream.write(String.format(
                                    "%-16s %-14.2f %-16s %-16s %-16s %s\n", "rcv",
                                    (System.currentTimeMillis() - STPPacketController.getStart()) / 1000f,
                                    (received_packet.getDataLength() > 0 ? "D"
                                            : ((received_packet.getHeaderController().getSYN() ? "S" : "")
                                                    + (received_packet.getHeaderController().getACK() ? "A" : "")
                                                    + (received_packet.getHeaderController().getFIN() ? "F" : ""))),
                                    received_packet.getHeaderController().getSequenceNumber(),
                                    received_packet.getDataLength(),
                                    received_packet.getHeaderController().getAcknowledgeNumber()).getBytes());
                        } catch (IOException e) {

                        }
                    }
                } else {
                    STPPacketController.timeout_retransmitted += 1;
                    sent_to_ack.peek().retransmitting();
                    sent_to_ack.peek().sendPacket();
                }
                break;
            }
            case FIN_WAIT_1: {
                boolean SYN = false;
                boolean RST = false;
                boolean ACK = false;
                boolean FIN = true;
                boolean bypass = true;
                STPPacketController packet = new STPPacketController(sequence_number, acknowledgement_number, SYN, RST,
                        ACK, FIN, null);
                packet.sendPacket();
                state = SenderState.FIN_WAIT_2;
                break;
            }
            case FIN_WAIT_2: {
                STPPacketController received_packet = STPPacketController.receivePacketAndLog(
                        getTimeout(getTimeInterval(estimated_RTT, dev_RTT, gamma), System.currentTimeMillis()));
                if (received_packet == null) {
                    state = SenderState.FIN_WAIT_1;
                } else if (received_packet.getHeaderController().getACK()
                        && received_packet.getHeaderController().getAcknowledgeNumber() == sequence_number + 1) {
                    state = SenderState.TIME_WAIT;
                }
                break;
            }
            case TIME_WAIT: {
                STPPacketController received_packet = STPPacketController.receivePacketAndLog(
                        getTimeout(getTimeInterval(estimated_RTT, dev_RTT, gamma), System.currentTimeMillis()));
                if (received_packet == null) {
                    state = SenderState.CLOSED;
                } else if (received_packet.getHeaderController().getFIN()
                        && received_packet.getHeaderController().getSequenceNumber() == acknowledgement_number) {
                    acknowledgement_number = received_packet.getHeaderController().getSequenceNumber() + 1;
                    sequence_number = received_packet.getHeaderController().getAcknowledgeNumber();
                    new STPPacketController(sequence_number, acknowledgement_number, false, false, true, false, null)
                            .sendPacket();
                    state = SenderState.CLOSED;
                    ;
                }
                break;
            }
            default:
                break;
            }
            if (previous_state != state) {
                System.out.println(state);
                previous_state = state;
            }
        }

        while (Thread.activeCount() > 1) {

        }

        try {
            STPPacketController.log_output_stream
                    .write("=============================================================\n".getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream
                    .write(String.format("%-50s %8d\n", "Size of the file (in Bytes)", total_file_size).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream
                    .write(String.format("%-50s %8d\n", "Segments transmitted (including drop & RXT)",
                            STPPacketController.transmitted + STPPacketController.dropped).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(
                    String.format("%-50s %8d\n", "Number of Segments handled by PLD", STPPacketController.pld_handled)
                            .getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(
                    String.format("%-50s %8d\n", "Number of Segments dropped", STPPacketController.dropped).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String
                    .format("%-50s %8d\n", "Number of Segments Corrupted", STPPacketController.corrupted).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String
                    .format("%-50s %8d\n", "Number of Segments Re-ordered", STPPacketController.reordered).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String
                    .format("%-50s %8d\n", "Number of Segments Duplicated", STPPacketController.duplicated).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(
                    String.format("%-50s %8d\n", "Number of Segments Delayed", STPPacketController.delayed).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String.format("%-50s %8d\n",
                    "Number of Retransmissions due to timeout", STPPacketController.timeout_retransmitted).getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String
                    .format("%-50s %8d\n", "Number of Fast Retransmissions", STPPacketController.fast_retransmitted)
                    .getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream.write(String
                    .format("%-50s %8d\n", "Number of Duplicate Acknowledgements received", STPPacketController.rcv_da)
                    .getBytes());
        } catch (IOException e) {

        }

        try {
            STPPacketController.log_output_stream
                    .write("=============================================================\n".getBytes());
        } catch (IOException e) {

        }
    }

    public static int getTimeout(long timeout, long sent_time) {
        return (int) (sent_time + timeout - System.currentTimeMillis()) <= 0 ? 1
                : (int) (sent_time + timeout - System.currentTimeMillis());
    }

    public static long getTimeInterval(long estimated_RTT, long dev_RTT, int gamma) {
        return estimated_RTT + gamma * dev_RTT;
    }

    public static long getEstimatedRTT(long estimated_RTT, long sample_RTT) {
        float alpha = 0.125f;
        return (long) ((1.0f - alpha) * estimated_RTT + alpha * sample_RTT);
    }

    public static long getDevRTT(long estimated_RTT, long dev_RTT, long sample_RTT) {
        float beta = 0.25f;
        return (long) ((1.0f - beta) * dev_RTT + beta * Math.abs(sample_RTT - estimated_RTT));
    }
}