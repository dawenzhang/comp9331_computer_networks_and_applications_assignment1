# COMP9331 Computer Networks and Applications Assignment 1 Semester 2, 2018

## a reliable byte-stream oriented transport protocol over UDP protocol with a packet loss and delay simulation module

### functionality

- A three-way handshake (SYN, SYN+ACK, ACK) for the connection establishment

- A four-segment (FIN, ACK, FIN, ACK) connection termination

- sender maintained single-timer for timeout operation

- fast retransmission

- maximum segment size and maximum window size

- log

- packet loss and delay simulation module

    the following rates can be specified:

    * packet drop rate

    * packet duplicate rate

    * packet corrupt rate

    * packet reorder rate

    * packet delay rate

### build

```
javac *.java
```

### run

- run sender, all fields are required:

```
java Sender receiver_host_ip receiver_port file_to_send MWS MSS gamma pDrop pDuplicate pCorrupt pOrder maxOrder pDelay maxDelay seed
```

e.g.

MSS = 100, MWS = 500, gamma = 4, drop rate = 10%

```
java Sender 127.0.0.1 8888 README.md 500 100 4 0.1 0 0 0 0 0 0 100
```

- run receiver, all fields are required:

```
java Receiver receiver_port file_to_receive
```

e.g.

```
java Receiver 8888 README_received.md
```

### __if you find this repo, please do not use any code fragment from it for academic assessment__